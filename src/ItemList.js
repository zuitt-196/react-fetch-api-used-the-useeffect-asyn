import React from 'react'
import LineItem from './LineItem';


// list and keys => when we talking about list and keys is define about the objec array whereass commonly to acces the data 
// list-> Ex of list in array obejct with properstise and values these is example of list
// key --> is the one handle the id of list which is to  modify the the array, while of used unique id .




// props and props drilling =We use props in React to pass data from one component to another (from a parent component to a child component(s)). Props is just a shorter way of saying properties. They are useful when you want the flow of data in your app to be dynamic

function ItemList({ items, handleCkeck, handleDelete }) {
    return (
        <div>
            < ul >
                {items.map((item) => (
                    <LineItem
                        key={item.id}
                        item={item}
                        handleCkeck={handleCkeck}
                        handleDelete={handleDelete}
                    />
                ))}
            </ul>
        </div>
    )
}

export default ItemList