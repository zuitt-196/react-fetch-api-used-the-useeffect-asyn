
import Header from './Header';
import Additem from './Additem';
import Content from './Content';
import Footer from './Footer';
import { useState, useEffect } from 'react';
import SearchItem from './SearchItem';
//  useState is allowed variable in functional components
// we shoud appropriate the conviction s

function App() {
  // initialize the varible API_URL with value of JSON server
  const API_URL = 'http://localhost:3500/items';


  //used of useState hooks which is the getter and setter
  //  items --> current value and variale of useState function, konow as a getter valueby components
  // setItems --> is setter or put existing value of items knows as setter
  // useState --> usestate function is initialized the current value item
  const [items, setItems] = useState([]);
  // define new state with empty value
  const [newItem, setINewtems] = useState('');
  // Define the searItem
  const [search, setSearch] = useState('');

  // Define to initialize the fetchError 
  const [fetchError, setFetchError] = useState(null);

  // Define to initialize the loading API with value of bollean or track the the API 
  const [isLoading, setIsLoading] = useState(true);

  // used by the useEffect we reDifine all components in the action 
  useEffect(() => {
    const fetchItems = async () => {
      try {
        const response = await fetch(API_URL);
        if (!response.ok) throw Error("Did not receive expected data")
        const listItems = await response.json();
        setItems(listItems);
        setFetchError(null);
      } catch (err) {
        setFetchError(err.message)
      } finally {
        setIsLoading(false);
      }
    }

    // invoke the the fetchItems() async with the value API 
    // might be simualation or track the async operation with 2000 mili seconds
    setTimeout(() => {
      (async () => await fetchItems())();
    }, 2000);


  }, [])

  //set the item array object
  // const setAndSaveItems = (newItems) => {
  //   setItems(newItems);

  // }


  // add new array object Item
  const addItem = (item) => {
    // console.log(items)
    const id = items.length ? items[items.length - 1].id + 1 : 1;
    const myNewItem = { id, checked: false, item };
    const listItems = [...items, myNewItem];
    setItems(listItems)
  }

  // handle check fucntion 
  const handleCkeck = (id) => {
    // declaration of varible with the value high order function which is map() method.
    // Along the use of Ternary conditiond
    const listItems = items.map((item) => item.id === id ? { ...item, checked: !item.checked } : item);
    setItems(listItems)


  }

  //handle delete function 
  const handleDelete = (id) => {
    // Use the high order function which filter() method which is trgger the item.id which have came to condition of 
    //Strict inequality to result the false boolean 
    const listItems = items.filter((item) => item.id !== id);
    // console.log(listItems);
    setItems(listItems);

  }

  // hold the newItem value or the handleSubmit function 
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!newItem) return;

    //additem 
    addItem(newItem);
    setINewtems('')

  }



  return (
    <div className="App">
      <Header title="Groceries list item" />
      <Additem
        newItem={newItem}
        setINewtems={setINewtems}
        handleSubmit={handleSubmit}
      />
      <SearchItem
        search={search}
        setSearch={setSearch}
      />
      {/* we filter it the list of items use the high order function which is filter method */}
      {/* Within the value of searchIt */}
      {/* Break the code Content component was filter in order to define thier value or find their value with the seacrh componets will be
        specified, tolowerCase() method is the one to know if the value of items is start of lowercase then access of incluedes() to determine if the value of searh components and items component if the same. 
        includes() method is return to bolean data 
          */ }

      <main>
        {isLoading && <p>Loadin Items...</p>}
        {fetchError && <p style={{ color: "red" }}>{`Error: ${fetchError}`}</p>}

        {!fetchError && !isLoading &&
          < Content
            items={items.filter(item => ((item.item).toLowerCase()).includes(search.toLowerCase()))}
            handleCkeck={handleCkeck}
            handleDelete={handleDelete}
          />}
      </main>
      <Footer length={items.length} />
    </div>
  );

}
export default App;
